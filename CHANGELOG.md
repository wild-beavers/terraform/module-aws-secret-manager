## 3.0.2

- maintenance: migrate `wild-beavers` modules to Terraform registry
- chore: bump pre-commit hooks

## 3.0.1

- fix: nothing new, just for pipeline re run

## 3.0.0

- feat: introduces `audit` scope through `var.iam_policy_entity_arns.audit` for auditing secrets and related KMS key
- feat: adds optional `var.testing_prefix` and `var.current_account_id`
- feat: adds `var.iam_policy_restrict_by_regions` to restrict policies by regions
- feat: adds `var.iam_policy_source_arns` to restrict policies by source ARNs
- feat: (BREAKING) deprecates the following variables and provide replacements:
   - `iam_policy_create` => *no_replacement*
   - `iam_policy_group_arns` => *no_replacement*
   - `iam_policy_external_resources_enabled` => *no_replacement*
   - `iam_policy_external_resources_arns` => `iam_policy_identity.resources_arns`
   - `iam_policy_source_policy_documents` => `iam_policy_identity.source_policy_documents`
   - `iam_policy_name_template` => `iam_policy_identity.name_template`
   - `iam_policy_description` => `iam_policy_identity.description_template`
   - `iam_policy_path` => `iam_policy_identity.path`
   - `iam_policy_tags` => `iam_policy_identity.tags`
   - `iam_policy_export_json` => `iam_policy_identity_export_jsons`
   - `secrets.json_policy` => `iam_policy_identity_export_jsons`
- feat: (BREAKING) removes the following outputs and provided replacements:
   - `iam_policies` => `aws_iam_policies`
   - `iam_policy_jsons` => `aws_iam_policies.xxx.json`
   - `iam_policy_actions` => `precomputed.aws_iam_policies.xxx.actions`
   - `secrets` => `aws_secretsmanager_secrets`
   - `kms_key` => `aws_kms_key`, `aws_kms_alias`, `replica_aws_kms_key` and `replica_aws_kms_alias`
- feat: do not automatically deprecated fetch groups members from `var.iam_policy_entity_arns` anymore
- feat: data like regions and current account ID can be passed as variables to prevent unneeded API calls

## 2.0.3

- fix: `recovery_window_in_days` could not be set to `0`

## 2.0.2

- fix: service policy scope statement creation when scope is only in `var.iam_policy_group_arns`
- chore: bump pre-commit hooks

## 2.0.1

- fix: remove unused `var.attach_to_principal`, as mentioned in README, no attachment happens in this module

## 2.0.0

- refactor: (BREAKING) changes IAM policy system to make it more generic:
   - (BREAKING) no more `policy_scope` for each secrets
   - (BREAKING) remove `iam_policy_role_arns` variable, replaced by `iam_policy_entity_arns`
- feat: (BREAKING) removes `var.replica_region` as it conflicts with `aws.replica` alias.
- feat: adds `secrets.arns` output containing main ARN and optional replica ARNS
- feat: adds `secrets_arns` output containing all secrets main ARN and optional replica ARNS
- fix: when scope is "RO", KMS policy should also be "RO"
- refactor: scope most code in `main.tf`
- test: adds checks for easier future code refactor
- test: rename `disabled` example to `disable`
- maintenance: pins pre-commit deps

## 1.3.1

- fix: when scope is "RO", KMS policy should also be "RO"
- fix: prevent idempotency issue by making KMS policy depends on data source

## 1.3.0

- feat: allows to add specific tags for a specific secret
- feat: accepts wildcards (and dynamic variables) for `iam_policy_user_arns` and `iam_policy_role_arns`
- fix: removes `JenkinsFile`

## 1.2.0

- feat: adds `iam_admin_arns` variable, to setup secrets administrator in internal policies.
- feat: makes sure object values for `iam_policy_`… can be null, because nulls are already filtered out
- fix: fixes internal policies that were not restricting anything before
- fix: marks secret value as sensitive
- fix: adds default `origin` tag on all resources

## 1.1.0

- feat: allow to set a RAW password (VS JSON) by setting the `raw` key
- fix: only create RW policy document if some secrets are RW
- fix: only create KMS RW policy document if some secrets are not RO
- chore: bump pre-commit hooks

## 1.0.0

- feat: (BREAKING) requires provider: `aws.replica`
- feat: (BREAKING) allows to pass multiple secrets, in a single `var.secrets` variable.
- feat: (BREAKING) bumps Terraform minimal version to `1.0.9`+
- feat: adds missing variables `rotation_schedule_expression` and `rotation_duration`
- feat: adds replica for secrets, with variables `replica_enabled`, `replica_region` and `replica_kms_key_id`
- feat: adds `var.kms_key` to create KMS (and replica KMS) for all secrets
- feat: adds `replica` output
- chore: bump pre-commit hooks

## 0.1.2

- maintenance: fix TFLINT issues
- chore: bump pre-commit hooks
- chore: change pre-commit hooks `git` scheme to `https`
- refactor: replace `count` by `for_each`
- test: adds gitlab ci
- doc: improve the README

## 0.1.1

- doc: terraform-docs update

## 0.1.0

- feat: adds secret manager, secret, policy and rotation

## 0.0.0

- Initial version
