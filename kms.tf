####
# Variables
####

variable "kms_key_id" {
  type        = string
  default     = null
  description = "ARN or ID of the AWS KMS customer master key (CMK) to be used to encrypt the resources data in the versions stored in this secret. If you don't specify this value or `var.kms_key`, then Secrets Manager defaults to using the AWS account's default CMK (the one named `aws/secretsmanager`). If the default KMS CMK with that name doesn't yet exist, then AWS Secrets Manager creates it for you automatically the first time."
}

variable "kms_key_replica_id" {
  type        = string
  default     = null
  description = "ARN or ID of the AWS KMS customer master key (CMK) replica to be used to encrypt the resources data in the versions stored in this secret. If you don't specify this value or `var.kms_key`, then Secrets Manager defaults to using the AWS account's default CMK (the one named `aws/secretsmanager`). If the default KMS CMK with that name doesn't yet exist, then AWS Secrets Manager creates it for you automatically the first time."
}

variable "kms_key" {
  description = <<-DOCUMENTATION
Options of the KMS key to create.
If given, `var.kms_key_id` will be ignored.

  - alias                   (required, string):      KMS key alias; Display name of the KMS key. Omit the `alias/`.
  - policy_json             (optional, string):      Additional policy to attach to the KMS key, merged with a baseline internal policy.
  - description             (optional, string, ""):  Description of the key.
  - rotation_enabled        (optional, bool, true):  Whether to automatically rotate the KMS key linked to the secrets.
  - deletion_window_in_days (optional, number, 7):   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.
  - tags                    (optional, map(string)): Tags to be used by the KMS key of this module.
DOCUMENTATION
  type = object({
    alias                   = string
    policy_json             = optional(string, null)
    description             = optional(string, "")
    rotation_enabled        = optional(bool, true)
    deletion_window_in_days = optional(number, 7)
    tags                    = optional(map(string), {})
  })
  default = null
}

####
# Locals
####

locals {
  kms_should_create             = var.kms_key != null
  kms_should_create_kms_replica = var.replica_enabled && local.kms_should_create

  kms_key_id         = local.kms_should_create ? module.kms["0"].aws_kms_aliases["0"].arn : var.kms_key_id
  kms_replica_key_id = local.kms_should_create_kms_replica ? module.kms["0"].replica_aws_kms_aliases["0"].arn : var.kms_key_replica_id
  kms_iam_ro_actions = [
    "kms:Decrypt",
  ]
  kms_iam_rw_actions = concat(
    [
      "kms:GenerateDataKey",
    ],
    local.kms_iam_ro_actions
  )
  kms_iam_audit_actions = [
    "kms:DescribeCustomKeyStores",
    "kms:DescribeKey",
    "kms:GetKeyPolicy",
    "kms:GetKeyRotationStatus",
    "kms:GetParametersForImport",
  ]
  kms_iam_actions = {
    ro    = local.kms_iam_ro_actions
    rw    = local.kms_iam_rw_actions
    rwd   = local.kms_iam_rw_actions
    audit = local.kms_iam_audit_actions
    full  = ["kms:*"]
  }
}

module "kms" {
  source  = "gitlab.com/wild-beavers/module-aws-kms/aws"
  version = "~> 2"

  for_each = local.kms_should_create ? { 0 = var.kms_key } : {}

  kms_keys = {
    0 = {
      alias             = each.value.alias
      principal_actions = local.kms_iam_actions
      service_principal = "secretsmanager"
      service_actions = concat(
        // https://docs.aws.amazon.com/secretsmanager/latest/userguide/security-encryption.html
        [
          "kms:Encrypt",
          "kms:DescribeKey",
        ],
        local.kms_iam_rw_actions
      )
      policy_json = each.value.policy_json

      description             = each.value.description
      rotation_enabled        = each.value.rotation_enabled
      deletion_window_in_days = each.value.deletion_window_in_days
      tags                    = each.value.tags
    }
  }

  replica_enabled = local.kms_should_create_kms_replica

  iam_policy_create                  = false
  iam_policy_export_json             = false
  iam_policy_entity_arns             = var.iam_policy_entity_arns
  iam_policy_source_arns             = var.iam_policy_source_arns
  iam_policy_sid_prefix              = var.iam_policy_sid_prefix
  iam_policy_restrict_by_account_ids = var.iam_policy_restrict_by_account_ids

  tags = local.tags

  providers = {
    aws.replica = aws.replica
  }
}

data "aws_iam_policy_document" "identity_kms" {
  for_each = local.iam_should_generate_identity_policy_documents ? local.iam_all_used_scopes : []

  source_policy_documents = var.iam_policy_source_policy_documents

  dynamic "statement" {
    for_each = local.kms_should_create ? { 0 = 0 } : {}

    content {
      sid       = "${chomp(var.iam_policy_sid_prefix)}KMS${upper(each.value)}"
      effect    = "Allow"
      actions   = local.kms_iam_actions[each.value]
      resources = module.kms["0"].precomputed.aws_kms_aliases["0"].arns
    }
  }
}

####
# Outputs
####

output "aws_kms_key" {
  value = local.kms_should_create ? module.kms["0"].aws_kms_keys["0"] : null
}

output "aws_kms_alias" {
  value = local.kms_should_create ? module.kms["0"].aws_kms_aliases["0"] : null
}

output "replica_aws_kms_key" {
  value = local.kms_should_create && local.kms_should_create_kms_replica ? module.kms["0"].replica_aws_kms_keys["0"] : null
}

output "replica_aws_kms_alias" {
  value = local.kms_should_create && local.kms_should_create_kms_replica ? module.kms["0"].replica_aws_kms_aliases["0"] : null
}
