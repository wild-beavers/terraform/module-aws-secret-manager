output "precomputed" {
  value = {
    aws_iam_policies = var.iam_policy_export_actions || var.iam_policy_identity != null ? { for scope in keys(local.iam_scoped_actions) :
      scope => merge(
        local.iam_should_generate_identity_policy ? lookup(module.iam_policy["0"].precomputed.aws_iam_policies, scope, null) : null,
        var.iam_policy_export_actions ? {
          actions = local.iam_scoped_actions[scope]
        } : null,
        var.iam_policy_export_actions ? {
          kms_actions = local.kms_iam_actions[scope]
        } : null
      )
    } : null
    aws_kms_aliases = local.kms_should_create ? module.kms["0"].precomputed.aws_kms_aliases : null
  }
}
