locals {
  current_account_id = var.current_account_id != "" ? var.current_account_id : data.aws_caller_identity.this["0"].account_id
  current_region     = var.current_region != "" ? var.current_region : data.aws_region.current["0"].name
  replica_region     = var.replica_region != "" ? var.replica_region : (var.replica_enabled ? data.aws_region.replica["0"].name : "")
}

data "aws_caller_identity" "this" {
  for_each = var.current_account_id == "" ? { 0 = "enabled" } : {}
}

data "aws_region" "current" {
  for_each = var.current_region == "" ? { 0 = "enabled" } : {}
}

data "aws_region" "replica" {
  for_each = var.replica_region == "" && var.replica_enabled ? { 0 = "enabled" } : {}

  provider = aws.replica
}
