####
# Variables
####

variable "iam_policy_identity" {
  description = <<-DOCUMENTATION
Identity-based IAM Policies to create.
Only policies fitting resource-based user-provided scopes (`ro`, `rw`, `rwd`, `audit` and `full`) will be created.
    - name_template           (required, string):           Name template of the IAM Policies. Expect a `%s` to be replace with the user-provided scope (`ro`, `rw`, `rwd`, `audit` and `full`).
    - description_template    (required, string):           Description template of the IAM Policies. Expect a `%s` to be replace with the user-provided scope (`ro`, `rw`, `rwd`, `audit` and `full`).
    - path                    (optional, string, "/"):      Path of the IAM Policies.
    - tags                    (optional, map(string), {}):  Tags of the IAM Policies. Will be merged with `var.tags`.
DOCUMENTATION
  type = object({
    name_template        = string
    description_template = string
    path                 = optional(string, "/")
    tags                 = optional(map(string), {})
  })
  default = null
}

variable "iam_policy_identity_document" {
  description = <<-DOCUMENTATION
Arguments specific for the identity-based Policy documents.
Mandatory if `var.iam_policy_identity`, but might be used solely in combination with `var.iam_policy_identity_export_jsons`.
    - source_policy_documents (optional, list(string), []): List of JSON strings to be concatenated to the identity-based polices.
    - resources_arns          (optional(list(string), []):  ARNs of resources created outside this module to compose the IAM identity-based Policies with.
DOCUMENTATION
  type = object({
    source_policy_documents = optional(list(string), [])
    resources_arns          = optional(list(string), [])
  })
  default = null
}

variable "iam_policy_identity_export_jsons" {
  description = "Whether to export identity-based IAM policies contents as JSON. Setting this to `true` do not create any policies. This can be used instead of or in combination with `var.external_iam_policy`."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_sid_prefix" {
  description = "Use a prefix for all `Sid` of all the policies created by this module."
  type        = string
  default     = null
}

variable "iam_policy_export_actions" {
  description = "Whether or not to export IAM policies actions. A lightweight way to generate your own policies."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_entity_arns" {
  description = "Restrict access to the key to the given IAM entities (roles or users). Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd` or `full`, each specifying the expected level of access for entities inside."
  type        = map(map(string))
  default     = {}
  nullable    = false

  validation {
    condition = alltrue([for key, arns in var.iam_policy_entity_arns : (
      alltrue([
        for arn in values(arns) : (
          arn == null || can(regex("^arn:aws(-us-gov|-cn)?:(iam|sts):([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws|\\*):(assumed-role|role|user|federated-user)/[a-zA-Z0-9+=,_:\\*\\./@{}'\\$\\?-]+$", arn))
      )])
      )
    ])
    error_message = "One or more “var.iam_policy_entity_arns” are invalid."
  }
}

variable "iam_policy_source_arns" {
  description = "Restrict accesses to the given source ARNs. Wildcards are allowed. Keys are scope: `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the expected level of access for entities defined within."
  type        = map(map(string))
  nullable    = false
  default     = {}
}

variable "iam_policy_restrict_by_regions" {
  description = "Restrict resource-based IAM Policy by the given regions. Resource-Based Policy will whitelist given list. If empty, no restriction will be applied. Especially useful when IAM entities contains region wildcards."
  type        = list(string)
  default     = []
  nullable    = false
}

variable "iam_policy_restrict_by_account_ids" {
  description = "Restrict IAM Policy by the given account IDs. Resource-based Policy will whitelist given list. If empty, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards."
  type        = list(string)
  default     = []
  nullable    = false

  validation {
    condition = alltrue([for id in var.iam_policy_restrict_by_account_ids : (
      can(regex("^[0-9]{12}$", id))
      )
    ])
    error_message = "One or more “var.iam_policy_restrict_by_account_ids” are invalid."
  }
}

variable "iam_policy_source_policy_documents" {
  description = "List of JSON strings to be concatenated to the resource-based IAM Policy."
  type        = list(string)
  default     = []
  nullable    = false
}

####
# Locals
####

locals {
  iam_should_generate_identity_policy           = var.iam_policy_identity != null
  iam_should_generate_identity_policy_documents = local.iam_should_generate_identity_policy || var.iam_policy_identity_export_jsons
}

####
# Resources
####

locals {
  iam_ro_actions = [
    "secretsmanager:GetSecretValue",
    "secretsmanager:DescribeSecret",
    "secretsmanager:ListSecretVersionIds",
  ]
  iam_rw_actions = concat(
    local.iam_ro_actions,
    [
      "secretsmanager:PutSecretValue",
      "secretsmanager:CancelRotateSecret",
      "secretsmanager:RestoreSecret",
      "secretsmanager:ReplicateSecretToRegions",
      "secretsmanager:StopReplicationToReplica",
      "secretsmanager:UpdateSecret",
      "secretsmanager:UpdateSecretVersionStage",
    ]
  )
  iam_rwd_actions = concat(
    local.iam_rw_actions,
    [
      "secretsmanager:RotateSecret",
      "secretsmanager:DeleteSecret",
    ]
  )
  iam_audit_actions = [
    "secretsmanager:DescribeSecret",
    "secretsmanager:GetResourcePolicy",
    "secretsmanager:ListSecretVersionIds",
    "secretsmanager:ListSecrets",
  ]
  iam_full_actions = [
    "secretsmanager:*",
  ]
  iam_scoped_actions = {
    ro    = local.iam_ro_actions
    rw    = local.iam_rw_actions
    rwd   = local.iam_rwd_actions
    audit = local.iam_audit_actions
    full  = local.iam_full_actions
  }

  iam_all_used_scopes = toset(distinct(concat(keys(var.iam_policy_entity_arns), keys(var.iam_policy_source_arns))))
  identity_iam_policy_document = var.iam_policy_identity_document != null || length(var.iam_policy_source_policy_documents) > 0 || var.iam_policy_identity_export_jsons ? {
    iam_policy_source_policy_documents = try(length(var.iam_policy_identity_document.source_policy_documents) > 0, false) ? var.iam_policy_identity_document.source_policy_documents : var.iam_policy_source_policy_documents
    scoped_source_policy_documents = { for scope, doc in data.aws_iam_policy_document.identity_kms :
      scope => [doc.json]
    }
    resources_arns = concat(try(var.iam_policy_identity_document.resources_arns, []), compact(concat(
      flatten([for secret in aws_secretsmanager_secret.this :
        compact([
          secret.arn,
          var.replica_enabled ? replace(secret.arn, local.current_region, local.replica_region) : null,
        ])
      ]))
    ))
  } : null
}

module "iam_policy" {
  source  = "gitlab.com/wild-beavers/module-aws-iam-policy/aws"
  version = "~> 1"

  for_each = length(local.secrets_with_policy) > 0 ? { 0 = "enabled" } : {}

  current_account_id = local.current_account_id
  testing_prefix     = var.testing_prefix
  tags               = var.tags

  entity_arns             = var.iam_policy_entity_arns
  resource_arns           = ["*"]
  scoped_actions          = local.iam_scoped_actions
  sid_prefix              = var.iam_policy_sid_prefix
  source_arns             = var.iam_policy_source_arns
  restrict_by_regions     = var.iam_policy_restrict_by_regions
  restrict_by_account_ids = var.iam_policy_restrict_by_account_ids
  source_policy_documents = var.iam_policy_source_policy_documents

  identity_iam_policy          = var.iam_policy_identity
  identity_iam_policy_document = local.identity_iam_policy_document
  identity_export_jsons        = var.iam_policy_identity_export_jsons
}

####
# Outputs
####

output "aws_iam_policies" {
  value = length(local.secrets_with_policy) > 0 && local.iam_should_generate_identity_policy_documents ? { for scope in local.iam_all_used_scopes :
    scope => merge(
      module.iam_policy["0"].aws_iam_policies[scope],
      var.iam_policy_identity_export_jsons ? {
        json = module.iam_policy["0"].aws_iam_policies[scope].json
      } : null
    )
  } : null
}
