####
# Variables
####

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Tags to be used by all resources of this module."
}

variable "secrets" {
  description = <<-DOCUMENTATION
Manage secrets in secret manager
Keys are free values.

  - name                              (required, string):    Name of the secret to create.
  - description                       (required, string):    A description of the secret to create.
  - recovery_window_in_days           (optional, number, 7): The number of days that AWS Secrets Manager waits before it can delete the secret. This value can be `0` to force deletion without recovery or range from `7` to `30` days.

  - secrets                           (optional, map(string)): key/values data that to encrypt and store in a new version of the secret.

  - rotation_enabled                  (optional, bool):       Whether to enable secret rotation. Needs `rotation_lambda_arn`
  - rotation_lambda_arn               (optional, string):     Specifies the ARN of the Lambda function that can rotate the secret. If `null`, the secret will not be rotated.
  - rotation_automatically_after_days (optional, number):     Specifies the number of days between automatic scheduled rotations of the secret. Needs `rotation_lambda_arn`.
  - rotation_schedule_expression      (optional, string):     A `cron()` or `rate()` expression that defines the schedule for rotating your secret. Either `automatically_after_days` or `schedule_expression` must be specified. Needs `rotation_lambda_arn`.
  - rotation_duration                 (optional, number, 24): The length of the rotation window in hours. For example, `3` for a three hour window. Needs `rotation_lambda_arn`.

  - tags                              (optional, map(string), null): If provided, will merge these tags along with `var.tags` and `var.secret_tags` for the specific secret
DOCUMENTATION
  type = map(object({
    name                              = string
    description                       = string
    recovery_window_in_days           = optional(number, 7)
    secrets                           = optional(map(string), {})
    rotation_enabled                  = optional(bool, false)
    rotation_lambda_arn               = optional(string)
    rotation_automatically_after_days = optional(number)
    rotation_schedule_expression      = optional(string)
    rotation_duration                 = optional(number, 24)
    policy_json                       = optional(string)
    tags                              = optional(map(string), null)
  }))
  default = null

  validation {
    condition = ((!contains([
      for key, secret in coalesce(var.secrets, {}) :
      (
        (1 <= length(secret.name) && length(secret.name) <= 256) &&
        (1 <= length(secret.description) && length(secret.description) <= 2048) &&
        (secret.recovery_window_in_days == null || 0 == coalesce(secret.recovery_window_in_days, 0) || (7 <= coalesce(secret.recovery_window_in_days, 7) && coalesce(secret.recovery_window_in_days, 7) <= 30)) &&
        (secret.rotation_automatically_after_days == null || (1 <= coalesce(secret.rotation_automatically_after_days, 1) && coalesce(secret.rotation_automatically_after_days, 1) <= 1000)) &&
        (secret.rotation_lambda_arn == null || can(regex("^arn:aws(-us-gov|-cn)?:lambda:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:[0-9]{12}:function:[^:]{1,64}$", secret.rotation_lambda_arn))) &&
        (secret.policy_json == null || try(jsondecode(secret.policy_json), null) != null)
      )
    ], false)))
    error_message = "One or more “var.secrets” are invalid. Check the requirements in the variables.tf file."
  }
}

variable "secret_tags" {
  type        = map(string)
  default     = null
  description = "Tags to be used by secrets of this module."
}

variable "replica_enabled" {
  description = "Whether to enable replica in another region for the secret."
  type        = bool
  default     = false
}

####
# Locals
####

locals {
  tags = merge(
    var.tags,
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-secret-manager"
    }
  )

  secrets_with_values = {
    for key, secret in var.secrets :
    key => secret if length(secret.secrets) > 0 ? true : false
  }
  secret_rotations = {
    for key, secret in var.secrets :
    key => secret if secret.rotation_enabled
  }
}

####
# Resources
####

resource "aws_secretsmanager_secret" "this" {
  for_each = var.secrets

  name                    = "${var.testing_prefix}${each.value.name}"
  description             = each.value.description
  kms_key_id              = local.kms_key_id
  recovery_window_in_days = each.value.recovery_window_in_days
  tags                    = merge(local.tags, var.secret_tags, each.value.tags)

  dynamic "replica" {
    for_each = var.replica_enabled ? { 0 = "enabled" } : {}

    content {
      kms_key_id = local.kms_replica_key_id
      region     = local.replica_region
    }
  }
}

resource "aws_secretsmanager_secret_version" "this" {
  for_each = local.secrets_with_values

  secret_id     = aws_secretsmanager_secret.this[each.key].id
  secret_string = sensitive(coalesce(lookup(each.value.secrets, "raw", null), jsonencode(each.value.secrets)))

  lifecycle {
    ignore_changes = [
      secret_string
    ]
  }
}

resource "aws_secretsmanager_secret_rotation" "this" {
  for_each = local.secret_rotations

  secret_id           = aws_secretsmanager_secret.this[each.key].id
  rotation_lambda_arn = each.value.rotation_lambda_arn

  rotation_rules {
    automatically_after_days = each.value.rotation_schedule_expression != null ? each.value.rotation_automatically_after_days : null
    duration                 = format("%dh", each.value.rotation_duration)
    schedule_expression      = each.value.rotation_automatically_after_days != null ? each.value.rotation_schedule_expression : null
  }
}

####
# Secret Policy
####

locals {
  secrets_with_policy = { for key, secret in var.secrets :
    key => secret if secret.policy_json != null || length(var.iam_policy_entity_arns) > 0 || length(var.iam_policy_source_arns) > 0
  }
}

resource "aws_secretsmanager_secret_policy" "this" {
  for_each = local.secrets_with_policy

  secret_arn = aws_secretsmanager_secret.this[each.key].arn

  policy = module.iam_policy["0"].resource_based_aws_iam_policy_document.json
}

####
# Outputs
####

output "aws_secretsmanager_secrets" {
  value = { for key, secret in aws_secretsmanager_secret.this :
    key => {
      id              = secret.id
      arn             = secret.arn
      arn_replication = var.replica_enabled ? replace(secret.arn, local.current_region, local.replica_region) : null
      arns = compact([
        secret.arn,
        var.replica_enabled ? replace(secret.arn, local.current_region, local.replica_region) : null
      ])
      rotation_enabled = try(aws_secretsmanager_secret_rotation.this["0"].rotation_enabled, false)
    }
  }
}
