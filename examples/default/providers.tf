provider "aws" {
  region     = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile

  dynamic "assume_role" {
    for_each = var.aws_assume_role != null ? [1] : []

    content {
      session_name = "terraform"
      role_arn     = var.aws_assume_role
    }
  }
}

provider "aws" {
  region     = "us-west-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile

  dynamic "assume_role" {
    for_each = var.aws_assume_role != null ? [1] : []

    content {
      session_name = "terraform"
      role_arn     = var.aws_assume_role
    }
  }

  alias = "replica"
}
