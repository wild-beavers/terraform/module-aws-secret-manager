#####
# Context
#####

data "aws_region" "current" {
}

data "aws_region" "replica" {
  provider = aws.replica
}

locals {
  prefix     = "${random_string.start_letter.result}${random_string.this.result}"
  caller_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

resource "aws_iam_role" "test" {
  name = "${local.prefix}tftestsecrets"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = ""
        Principal = { AWS = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root" }
      },
    ]
  })
}

data "aws_caller_identity" "current" {}

#####
# Complete
#####

resource "aws_iam_user" "complete1" {
  name = "${local.prefix}secretcomplete1"
}

resource "aws_iam_user" "complete2" {
  name = "${local.prefix}secretcomplete2"
}

module "complete" {
  source = "../../"

  current_account_id = data.aws_caller_identity.current.account_id
  current_region     = data.aws_region.current.name
  replica_region     = data.aws_region.replica.name

  secrets = {
    empty = {
      name                    = "${local.prefix}tftestcomplete0"
      description             = "Empty with policy and tag"
      recovery_window_in_days = 0
      tags = {
        empty = "yes"
      }
    }

    single = {
      name                    = "${local.prefix}tftestcomplete1"
      description             = "Simple empty"
      recovery_window_in_days = 0
    }

    json = {
      name                    = "${local.prefix}tftestcomplete2"
      description             = "With JSON value and recovery windows"
      recovery_window_in_days = 0
      secrets = {
        username = "coco"
        password = "chanel"
      }
    }

    policy = {
      name        = "${local.prefix}tftestcomplete3"
      description = "With policy and raw non-json value."
      secrets     = { raw = "foobarbaz" }
    }
  }

  secret_tags = {
    purpose = "tftestSecret"
  }

  kms_key = {
    alias                   = "${local.prefix}tftestcomplete"
    description             = "${local.prefix}tftestcomplete KMS key example for Secret Manager"
    rotation_enabled        = true
    deletion_window_in_days = 7
    tags = {
      purpose = "tftestKMS"
    }
  }

  iam_policy_source_policy_documents = [data.aws_iam_policy_document.restrict_by_tags.json]
  iam_policy_entity_arns = {
    ro = {
      0 = aws_iam_user.complete1.arn
    }
    rw = {
      0 = aws_iam_role.test.arn,
      1 = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AnotherRole"
    }
    rwd = {
      0 = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RWDRole"
    }
    full = {
      call        = local.caller_arn,
      random_role = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RandomRole"
    }
  }
  iam_policy_sid_prefix     = local.prefix
  iam_policy_export_actions = true

  replica_enabled = true

  tags = {
    tftest = "tftest"
  }

  providers = {
    aws.replica = aws.replica
  }
}

data "aws_iam_policy_document" "restrict_by_tags" {
  statement {
    sid       = "AllowTags"
    effect    = "Deny"
    actions   = ["secretsmanager:*"]
    resources = ["*"]
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
    condition {
      test     = "StringNotEquals"
      values   = ["AUTHORIZED"]
      variable = "aws:PrincipalTag/myTag"
    }
    condition {
      test     = "StringNotLike"
      values   = [local.caller_arn]
      variable = "aws:principalArn"
    }
  }
}

#####
# Policies
#####

module "policies" {
  source = "../../"

  secrets = {
    policies = {
      name        = "${local.prefix}tftestpolicies"
      description = "Policies"
    }
  }

  replica_enabled = true

  iam_policy_identity = {
    name_template        = "${local.prefix}tftestpolicies%s"
    description_template = "%s ${local.prefix}tftestpolicies for secret manager"
  }
  iam_policy_sid_prefix              = local.prefix
  iam_policy_identity_export_jsons   = true
  iam_policy_restrict_by_account_ids = [data.aws_caller_identity.current.account_id]
  iam_policy_entity_arns = {
    rw = {
      0 = aws_iam_user.complete1.arn
      1 = aws_iam_role.test.arn,
      2 = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AnotherRole"
    }
    full = { caller = local.caller_arn }
  }
  iam_policy_source_arns = {
    rw = {
      0 = "arn:aws:service::${data.aws_caller_identity.current.account_id}:service/Somewhere"
    }
  }

  kms_key = {
    alias       = "${local.prefix}tftestpolicies"
    description = "${local.prefix}tftestpolicies KMS key example for Secret Manager"
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# External
#####

resource "aws_kms_key" "external" {
  description             = "${local.prefix}tftestexternal"
  deletion_window_in_days = 7
}

resource "aws_secretsmanager_secret" "external" {
  name = "${local.prefix}tftestexternal-ext"
}

resource "aws_kms_key" "external_replica" {
  description             = "${local.prefix}tftestexternalreplica"
  deletion_window_in_days = 7

  provider = aws.replica
}

data "aws_iam_policy_document" "external" {
  statement {
    sid       = "RotateOnlyPolicyExample"
    effect    = "Allow"
    actions   = ["secretsmanager:RotateSecret"]
    resources = [aws_secretsmanager_secret.external.arn]
  }
}

module "external" {
  source = "../../"

  secrets = {
    external_test = {
      name        = "${local.prefix}tftestexternal"
      description = "External secret"
      secrets     = { 0 = "ThisIsAKMSSecret?" }
    }
  }

  kms_key_id         = aws_kms_key.external.id
  kms_key_replica_id = aws_kms_key.external_replica.id

  iam_policy_identity = {
    name_template        = "${local.prefix}tftestexternal%s"
    description_template = "%s ${local.prefix}tftestexternal for secret manager"
  }
  iam_policy_identity_document = {
    source_policy_documents = [data.aws_iam_policy_document.external.json]
    resources_arns          = [aws_secretsmanager_secret.external.arn]
  }
  iam_policy_entity_arns = {
    full = {
      call        = local.caller_arn,
      random_role = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RandomRole"
    }
  }

  replica_enabled = true

  providers = {
    aws.replica = aws.replica
  }
}

#####
# Minimal
#####

module "minimal" {
  source = "../../"

  secrets = {
    minimal = {
      name        = "${local.prefix}tftestminimal"
      description = "Minimal secret"
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}
