output "complete" {
  value = module.complete
}

output "policies" {
  value = module.policies
}

output "external" {
  value = module.external
}

output "minimal" {
  value = module.minimal
}
