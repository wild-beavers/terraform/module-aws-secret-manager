#####
# Complete
#####

data "aws_kms_key" "complete" {
  key_id = module.complete.aws_kms_key.id

  depends_on = [
    module.complete
  ]
}

data "aws_secretsmanager_secret" "complete" {
  for_each = module.complete.aws_secretsmanager_secrets

  arn = each.value.arn

  depends_on = [
    module.complete
  ]
}

check "complete" {
  assert {
    condition = (
      alltrue([for key, secret in data.aws_secretsmanager_secret.complete : (
        can(regex(data.aws_kms_key.complete.multi_region_configuration[0].primary_key[0].region, secret.arn)) &&
        module.complete.aws_kms_alias.arn == secret.kms_key_id
      )])
    )
    error_message = "Check fail: KMS Replica/Secret"
  }

  assert {
    condition = alltrue([for name, secret in data.aws_secretsmanager_secret.complete : (
      jsondecode(secret.policy)["Statement"][0]["Action"] == "secretsmanager:*" &&
      jsondecode(secret.policy)["Statement"][0]["Condition"]["StringNotEquals"]["aws:PrincipalTag/myTag"] == "AUTHORIZED" &&
      jsondecode(secret.policy)["Statement"][0]["Effect"] == "Deny" &&
      jsondecode(secret.policy)["Statement"][0]["Principal"]["AWS"] == "*"
    )])
    error_message = "Check fail: resource-based custom statement"
  }

  assert {
    condition = alltrue([for name, secret in data.aws_secretsmanager_secret.complete : (
      jsondecode(secret.policy)["Statement"][1]["Action"] == "secretsmanager:*" &&
      jsondecode(secret.policy)["Statement"][1]["Effect"] == "Deny" &&
      jsondecode(secret.policy)["Statement"][1]["Principal"]["AWS"] == "*" &&
      contains(jsondecode(secret.policy)["Statement"][1]["Condition"]["StringNotLike"]["aws:principalArn"], aws_iam_user.complete1.arn) &&
      contains(jsondecode(secret.policy)["Statement"][1]["Condition"]["StringNotLike"]["aws:principalArn"], aws_iam_role.test.arn) &&
      contains(jsondecode(secret.policy)["Statement"][1]["Condition"]["StringNotLike"]["aws:principalArn"], local.caller_arn) &&
      contains(jsondecode(secret.policy)["Statement"][1]["Condition"]["StringNotLike"]["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RandomRole") &&
      contains(jsondecode(secret.policy)["Statement"][1]["Condition"]["StringNotLike"]["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AnotherRole") &&
      contains(jsondecode(secret.policy)["Statement"][1]["Condition"]["StringNotLike"]["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RWDRole")
    )])
    error_message = "Check fail: resource-based Whitelist statement"
  }

  assert {
    condition = alltrue([for name, secret in data.aws_secretsmanager_secret.complete : (
      contains(jsondecode(secret.policy)["Statement"][2]["NotAction"], "secretsmanager:ListSecretVersionIds") &&
      contains(jsondecode(secret.policy)["Statement"][2]["NotAction"], "secretsmanager:GetSecretValue") &&
      contains(jsondecode(secret.policy)["Statement"][2]["NotAction"], "secretsmanager:DescribeSecret") &&
      jsondecode(secret.policy)["Statement"][2]["Effect"] == "Deny" &&
      jsondecode(secret.policy)["Statement"][2]["Principal"]["AWS"] == "*" &&
      length(jsondecode(secret.policy)["Statement"][2]["Condition"]["StringNotLike"]["aws:principalArn"]) == 2 &&
      contains(jsondecode(secret.policy)["Statement"][2]["Condition"]["StringNotLike"]["aws:principalArn"], local.caller_arn) &&
      contains(jsondecode(secret.policy)["Statement"][2]["Condition"]["StringNotLike"]["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RandomRole") &&
      jsondecode(secret.policy)["Statement"][2]["Condition"]["StringLike"]["aws:principalArn"] == aws_iam_user.complete1.arn
    )])
    error_message = "Check fail: resource-based Restrict RO statement"
  }

  assert {
    condition = alltrue([for name, secret in data.aws_secretsmanager_secret.complete : (
      contains(jsondecode(secret.policy)["Statement"][3]["NotAction"], "secretsmanager:ListSecretVersionIds") &&
      contains(jsondecode(secret.policy)["Statement"][3]["NotAction"], "secretsmanager:GetSecretValue") &&
      contains(jsondecode(secret.policy)["Statement"][3]["NotAction"], "secretsmanager:DescribeSecret") &&
      contains(jsondecode(secret.policy)["Statement"][3]["NotAction"], "secretsmanager:PutSecretValue") &&
      contains(jsondecode(secret.policy)["Statement"][3]["NotAction"], "secretsmanager:RestoreSecret") &&
      contains(jsondecode(secret.policy)["Statement"][3]["NotAction"], "secretsmanager:UpdateSecret") &&
      jsondecode(secret.policy)["Statement"][3]["Effect"] == "Deny" &&
      jsondecode(secret.policy)["Statement"][3]["Principal"]["AWS"] == "*" &&
      length(jsondecode(secret.policy)["Statement"][3]["Condition"]["StringNotLike"]["aws:principalArn"]) == 2 &&
      contains(jsondecode(secret.policy)["Statement"][3]["Condition"]["StringNotLike"]["aws:principalArn"], local.caller_arn) &&
      contains(jsondecode(secret.policy)["Statement"][3]["Condition"]["StringNotLike"]["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RandomRole") &&
      length(jsondecode(secret.policy)["Statement"][3]["Condition"]["StringLike"]["aws:principalArn"]) == 2 &&
      contains(jsondecode(secret.policy)["Statement"][3]["Condition"]["StringLike"]["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AnotherRole") &&
      contains(jsondecode(secret.policy)["Statement"][3]["Condition"]["StringLike"]["aws:principalArn"], aws_iam_role.test.arn)
    )])
    error_message = "Check fail: resource-based Restrict RW statement"
  }

  assert {
    condition = alltrue([for name, secret in data.aws_secretsmanager_secret.complete : (
      contains(jsondecode(secret.policy)["Statement"][4]["NotAction"], "secretsmanager:ListSecretVersionIds") &&
      contains(jsondecode(secret.policy)["Statement"][4]["NotAction"], "secretsmanager:GetSecretValue") &&
      contains(jsondecode(secret.policy)["Statement"][4]["NotAction"], "secretsmanager:DescribeSecret") &&
      contains(jsondecode(secret.policy)["Statement"][4]["NotAction"], "secretsmanager:PutSecretValue") &&
      contains(jsondecode(secret.policy)["Statement"][4]["NotAction"], "secretsmanager:RestoreSecret") &&
      contains(jsondecode(secret.policy)["Statement"][4]["NotAction"], "secretsmanager:DeleteSecret") &&
      jsondecode(secret.policy)["Statement"][4]["Effect"] == "Deny" &&
      jsondecode(secret.policy)["Statement"][4]["Principal"]["AWS"] == "*" &&
      length(jsondecode(secret.policy)["Statement"][4]["Condition"]["StringNotLike"]["aws:principalArn"]) == 2 &&
      contains(jsondecode(secret.policy)["Statement"][4]["Condition"]["StringNotLike"]["aws:principalArn"], local.caller_arn) &&
      contains(jsondecode(secret.policy)["Statement"][4]["Condition"]["StringNotLike"]["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RandomRole") &&
      jsondecode(secret.policy)["Statement"][4]["Condition"]["StringLike"]["aws:principalArn"] == "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RWDRole"
    )])
    error_message = "Check fail: resource-based Restrict RWD statement"
  }

  assert {
    condition = (
      alltrue([
        for key, secret in data.aws_secretsmanager_secret.complete :
        (
          secret.tags["managed-by"] == "terraform" &&
          secret.tags["origin"] == "gitlab.com/wild-beavers/terraform/module-aws-secret-manager" &&
          secret.tags["purpose"] == "tftestSecret" &&
          secret.tags["tftest"] == "tftest"
        )
      ]) &&
      data.aws_secretsmanager_secret.complete["empty"].tags.empty == "yes" &&
      lookup(data.aws_secretsmanager_secret.complete["single"].tags, "empty", null) == null
    )
    error_message = "Check fail: secrets tags"
  }

  assert {
    condition = (
      data.aws_secretsmanager_secret.complete["empty"].description == "Empty with policy and tag" &&
      data.aws_secretsmanager_secret.complete["single"].description == "Simple empty" &&
      data.aws_secretsmanager_secret.complete["json"].description == "With JSON value and recovery windows" &&
      data.aws_secretsmanager_secret.complete["policy"].description == "With policy and raw non-json value."
    )
    error_message = "Check fail: secrets description"
  }

  assert {
    condition = alltrue([for name, secret in module.complete.aws_secretsmanager_secrets : (
      contains(secret.arns, data.aws_secretsmanager_secret.complete[name].arn) &&
      contains(secret.arns, secret.arn_replication) &&
      secret.arn == data.aws_secretsmanager_secret.complete[name].arn
    )])
    error_message = "Check fail: secrets outputs"
  }

  assert {
    condition = (
      length(module.complete.precomputed.aws_iam_policies) == 5 &&
      contains(module.complete.precomputed.aws_iam_policies.rwd.actions, "secretsmanager:PutSecretValue") &&
      contains(module.complete.precomputed.aws_iam_policies.rwd.kms_actions, "kms:Decrypt") &&
      contains(module.complete.precomputed.aws_kms_aliases["0"].arns, module.complete.aws_kms_alias.arn) &&
      contains(module.complete.precomputed.aws_kms_aliases["0"].arns, module.complete.replica_aws_kms_alias.arn)
    )
    error_message = "Check fail: complete outputs"
  }
}

#####
# Policies
#####

data "aws_iam_policy" "policies" {
  for_each = module.policies.aws_iam_policies

  name = each.value.name

  depends_on = [
    module.policies
  ]
}

check "policies" {
  assert {
    condition = (
      jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][1]["Effect"] == "Allow" &&
      contains(jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][1]["Action"], "secretsmanager:PutSecretValue") &&
      length(jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][1]["Resource"]) == length(module.policies.aws_secretsmanager_secrets) * 2 &&
      alltrue([for key, secret in module.policies.aws_secretsmanager_secrets : (
        contains(jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][1]["Resource"], secret.arn) &&
        contains(jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][1]["Resource"], secret.arn_replication)
      )]) &&
      jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][0]["Effect"] == "Allow" &&
      contains(jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][0]["Action"], "kms:GenerateDataKey") &&
      contains(jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][0]["Action"], "kms:Decrypt") &&
      length(jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][0]["Resource"]) == 2 &&
      contains(jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][0]["Resource"], module.policies.aws_kms_alias.arn) &&
      contains(jsondecode(module.policies.aws_iam_policies.rw.json)["Statement"][0]["Resource"], module.policies.replica_aws_kms_alias.arn)
    )
    error_message = "Check fail: policies json output RW"
  }

  assert {
    condition = (
      jsondecode(module.policies.aws_iam_policies.full.json)["Statement"][1]["Effect"] == "Allow" &&
      jsondecode(module.policies.aws_iam_policies.full.json)["Statement"][1]["Action"] == "secretsmanager:*" &&
      length(jsondecode(module.policies.aws_iam_policies.full.json)["Statement"][1]["Resource"]) == length(module.policies.aws_secretsmanager_secrets) * 2 &&
      alltrue([for key, secret in module.policies.aws_secretsmanager_secrets : (
        contains(jsondecode(module.policies.aws_iam_policies.full.json)["Statement"][1]["Resource"], secret.arn) &&
        contains(jsondecode(module.policies.aws_iam_policies.full.json)["Statement"][1]["Resource"], secret.arn_replication)
      )]) &&
      jsondecode(module.policies.aws_iam_policies.full.json)["Statement"][0]["Effect"] == "Allow" &&
      jsondecode(module.policies.aws_iam_policies.full.json)["Statement"][0]["Action"] == "kms:*" &&
      length(jsondecode(module.policies.aws_iam_policies.full.json)["Statement"][0]["Resource"]) == 2 &&
      contains(jsondecode(module.policies.aws_iam_policies.full.json)["Statement"][0]["Resource"], module.policies.aws_kms_alias.arn) &&
      contains(jsondecode(module.policies.aws_iam_policies.full.json)["Statement"][0]["Resource"], module.policies.replica_aws_kms_alias.arn)
    )
    error_message = "Check fail: policies json output FULL"
  }

  assert {
    condition = (
      alltrue([for scope, attributes in module.policies.aws_iam_policies : (
        attributes.arn == module.policies.precomputed.aws_iam_policies[scope].arn &&
        attributes.name == module.policies.precomputed.aws_iam_policies[scope].name
      )]) &&
      length(setsubtract(module.policies.precomputed.aws_kms_aliases["0"].arns, [module.policies.aws_kms_alias.arn, module.policies.replica_aws_kms_alias.arn])) == 0
    )
    error_message = "Check fail: policies precomputed outputs"
  }

  assert {
    condition = (
      length(setunion(keys(module.policies.aws_iam_policies), ["rw", "full"])) == 2 &&
      alltrue([for key, policy in data.aws_iam_policy.policies :
        (
          length(jsondecode(module.policies.aws_iam_policies[key].json).Statement) == 2 &&
          module.policies.aws_iam_policies[key].arn == policy.arn &&
          module.policies.aws_iam_policies[key].policy_id == policy.policy_id &&
          jsondecode(module.policies.aws_iam_policies[key].json) == jsondecode(policy.policy)
        )
      ])
    )
    error_message = "Check fail: policies outputs"
  }
}

#####
# Minimal
#####

check "minimal" {
  assert {
    condition = (
      module.minimal.aws_iam_policies == null &&
      module.minimal.precomputed.aws_iam_policies == null &&
      module.minimal.precomputed.aws_kms_aliases == null &&
      module.minimal.aws_kms_key == null &&
      module.minimal.aws_kms_alias == null &&
      module.minimal.replica_aws_kms_key == null &&
      module.minimal.replica_aws_kms_alias == null &&
      module.minimal.aws_secretsmanager_secrets["minimal"].arn_replication == null
    )
    error_message = "Check fail: minimal json outputs"
  }
}
