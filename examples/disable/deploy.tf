#####
# Disabled
#####

module "disabled" {
  source = "../../"

  count = 0

  providers = {
    aws.replica = aws.replica
  }
}
