# terraform-module-aws-secret-manager

This module manage a secret via AWS Secret Manager.
It optionally managed the secret value, the secret rotation, the resource-based secret policy, KMS keys and replicas.

## Limitations

- This module do not attach external policies to any entities, because of how hard the AWS provider (5+) makes it to attach policies to unknown entities dynamically.
- This module do not handle IAM Group members attachments, as per AWS limitation on the matter.
- This module provides a single resource-based policy; scoped identity-based policies shared with all secrets created by this module.
  To create various policies for different secrets, call this module multiple times.
  On the contrary, to make concise policies for multiple secrets, it’s recommended to call this module once.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.3.9 |
| aws | >= 3.15 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.15 |
| aws.replica | >= 3.15 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| iam\_policy | gitlab.com/wild-beavers/module-aws-iam-policy/aws | ~> 1 |
| kms | gitlab.com/wild-beavers/module-aws-kms/aws | ~> 2 |

## Resources

| Name | Type |
|------|------|
| [aws_secretsmanager_secret.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret) | resource |
| [aws_secretsmanager_secret_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_policy) | resource |
| [aws_secretsmanager_secret_rotation.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_rotation) | resource |
| [aws_secretsmanager_secret_version.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret_version) | resource |
| [aws_caller_identity.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.identity_kms](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |
| [aws_region.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| current\_account\_id | The account where this module is run. | `string` | `""` | no |
| current\_region | The region where this module is run. | `string` | `""` | no |
| iam\_policy\_entity\_arns | Restrict access to the key to the given IAM entities (roles or users). Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd` or `full`, each specifying the expected level of access for entities inside. | `map(map(string))` | `{}` | no |
| iam\_policy\_export\_actions | Whether or not to export IAM policies actions. A lightweight way to generate your own policies. | `bool` | `false` | no |
| iam\_policy\_identity | Identity-based IAM Policies to create.<br/>Only policies fitting resource-based user-provided scopes (`ro`, `rw`, `rwd`, `audit` and `full`) will be created.<br/>    - name\_template           (required, string):           Name template of the IAM Policies. Expect a `%s` to be replace with the user-provided scope (`ro`, `rw`, `rwd`, `audit` and `full`).<br/>    - description\_template    (required, string):           Description template of the IAM Policies. Expect a `%s` to be replace with the user-provided scope (`ro`, `rw`, `rwd`, `audit` and `full`).<br/>    - path                    (optional, string, "/"):      Path of the IAM Policies.<br/>    - tags                    (optional, map(string), {}):  Tags of the IAM Policies. Will be merged with `var.tags`. | <pre>object({<br/>    name_template        = string<br/>    description_template = string<br/>    path                 = optional(string, "/")<br/>    tags                 = optional(map(string), {})<br/>  })</pre> | `null` | no |
| iam\_policy\_identity\_document | Arguments specific for the identity-based Policy documents.<br/>Mandatory if `var.iam_policy_identity`, but might be used solely in combination with `var.iam_policy_identity_export_jsons`.<br/>    - source\_policy\_documents (optional, list(string), []): List of JSON strings to be concatenated to the identity-based polices.<br/>    - resources\_arns          (optional(list(string), []):  ARNs of resources created outside this module to compose the IAM identity-based Policies with. | <pre>object({<br/>    source_policy_documents = optional(list(string), [])<br/>    resources_arns          = optional(list(string), [])<br/>  })</pre> | `null` | no |
| iam\_policy\_identity\_export\_jsons | Whether to export identity-based IAM policies contents as JSON. Setting this to `true` do not create any policies. This can be used instead of or in combination with `var.external_iam_policy`. | `bool` | `false` | no |
| iam\_policy\_restrict\_by\_account\_ids | Restrict IAM Policy by the given account IDs. Resource-based Policy will whitelist given list. If empty, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards. | `list(string)` | `[]` | no |
| iam\_policy\_restrict\_by\_regions | Restrict resource-based IAM Policy by the given regions. Resource-Based Policy will whitelist given list. If empty, no restriction will be applied. Especially useful when IAM entities contains region wildcards. | `list(string)` | `[]` | no |
| iam\_policy\_sid\_prefix | Use a prefix for all `Sid` of all the policies created by this module. | `string` | `null` | no |
| iam\_policy\_source\_arns | Restrict accesses to the given source ARNs. Wildcards are allowed. Keys are scope: `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the expected level of access for entities defined within. | `map(map(string))` | `{}` | no |
| iam\_policy\_source\_policy\_documents | List of JSON strings to be concatenated to the resource-based IAM Policy. | `list(string)` | `[]` | no |
| kms\_key | Options of the KMS key to create.<br/>If given, `var.kms_key_id` will be ignored.<br/><br/>  - alias                   (required, string):      KMS key alias; Display name of the KMS key. Omit the `alias/`.<br/>  - policy\_json             (optional, string):      Additional policy to attach to the KMS key, merged with a baseline internal policy.<br/>  - description             (optional, string, ""):  Description of the key.<br/>  - rotation\_enabled        (optional, bool, true):  Whether to automatically rotate the KMS key linked to the secrets.<br/>  - deletion\_window\_in\_days (optional, number, 7):   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.<br/>  - tags                    (optional, map(string)): Tags to be used by the KMS key of this module. | <pre>object({<br/>    alias                   = string<br/>    policy_json             = optional(string, null)<br/>    description             = optional(string, "")<br/>    rotation_enabled        = optional(bool, true)<br/>    deletion_window_in_days = optional(number, 7)<br/>    tags                    = optional(map(string), {})<br/>  })</pre> | `null` | no |
| kms\_key\_id | ARN or ID of the AWS KMS customer master key (CMK) to be used to encrypt the resources data in the versions stored in this secret. If you don't specify this value or `var.kms_key`, then Secrets Manager defaults to using the AWS account's default CMK (the one named `aws/secretsmanager`). If the default KMS CMK with that name doesn't yet exist, then AWS Secrets Manager creates it for you automatically the first time. | `string` | `null` | no |
| kms\_key\_replica\_id | ARN or ID of the AWS KMS customer master key (CMK) replica to be used to encrypt the resources data in the versions stored in this secret. If you don't specify this value or `var.kms_key`, then Secrets Manager defaults to using the AWS account's default CMK (the one named `aws/secretsmanager`). If the default KMS CMK with that name doesn't yet exist, then AWS Secrets Manager creates it for you automatically the first time. | `string` | `null` | no |
| replica\_enabled | Whether to enable replica in another region for the secret. | `bool` | `false` | no |
| replica\_region | The region where this module must replicate resources. | `string` | `""` | no |
| secret\_tags | Tags to be used by secrets of this module. | `map(string)` | `null` | no |
| secrets | Manage secrets in secret manager<br/>Keys are free values.<br/><br/>  - name                              (required, string):    Name of the secret to create.<br/>  - description                       (required, string):    A description of the secret to create.<br/>  - recovery\_window\_in\_days           (optional, number, 7): The number of days that AWS Secrets Manager waits before it can delete the secret. This value can be `0` to force deletion without recovery or range from `7` to `30` days.<br/><br/>  - secrets                           (optional, map(string)): key/values data that to encrypt and store in a new version of the secret.<br/><br/>  - rotation\_enabled                  (optional, bool):       Whether to enable secret rotation. Needs `rotation_lambda_arn`<br/>  - rotation\_lambda\_arn               (optional, string):     Specifies the ARN of the Lambda function that can rotate the secret. If `null`, the secret will not be rotated.<br/>  - rotation\_automatically\_after\_days (optional, number):     Specifies the number of days between automatic scheduled rotations of the secret. Needs `rotation_lambda_arn`.<br/>  - rotation\_schedule\_expression      (optional, string):     A `cron()` or `rate()` expression that defines the schedule for rotating your secret. Either `automatically_after_days` or `schedule_expression` must be specified. Needs `rotation_lambda_arn`.<br/>  - rotation\_duration                 (optional, number, 24): The length of the rotation window in hours. For example, `3` for a three hour window. Needs `rotation_lambda_arn`.<br/><br/>  - tags                              (optional, map(string), null): If provided, will merge these tags along with `var.tags` and `var.secret_tags` for the specific secret | <pre>map(object({<br/>    name                              = string<br/>    description                       = string<br/>    recovery_window_in_days           = optional(number, 7)<br/>    secrets                           = optional(map(string), {})<br/>    rotation_enabled                  = optional(bool, false)<br/>    rotation_lambda_arn               = optional(string)<br/>    rotation_automatically_after_days = optional(number)<br/>    rotation_schedule_expression      = optional(string)<br/>    rotation_duration                 = optional(number, 24)<br/>    policy_json                       = optional(string)<br/>    tags                              = optional(map(string), null)<br/>  }))</pre> | `null` | no |
| tags | Tags to be used by all resources of this module. | `map(string)` | `{}` | no |
| testing\_prefix | Prefix to be used for all resources names. Specifically useful for tests. A 4-characters alphanumeric string. It must start by a lowercase letter. | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| aws\_iam\_policies | n/a |
| aws\_kms\_alias | n/a |
| aws\_kms\_key | n/a |
| aws\_secretsmanager\_secrets | n/a |
| precomputed | n/a |
| replica\_aws\_kms\_alias | n/a |
| replica\_aws\_kms\_key | n/a |
<!-- END_TF_DOCS -->

## Versioning

This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks

This repository uses [pre-commit](https://pre-commit.com/) hooks.
